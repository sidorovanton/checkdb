<?php
include("Core.php");
$Application = new Application("localhost","spicy","root","");
// Creating $arResult
if (isset($_POST["table_name"])){
    $table_name = $_POST["table_name"];
    $arResult = $Application->getResult($table_name);
}
else if (isset($_POST["open"])){
    $filename = $_SERVER["DOCUMENT_ROOT"]."/files/".$_POST["file_name"].".json";
    if(file_exists($filename)) {
        $data = file_get_contents($filename);
        $arResult = json_decode($data, true);
    }
    else return;
}
else
    $arResult = $Application->getResult();

if (isset($_POST["options"])){
    $tables = $Application->getTables();
    echo '<option value="all">ALL</option>';
        foreach ($tables as $key => $val):
            echo '<option value="'.$val[0].'">'.$val[0].'</option>';
        endforeach;
}
if (isset($_POST["save"])){
    $filename = $_SERVER["DOCUMENT_ROOT"]."/files/".$_POST["file_name"].".json";
    $data = json_encode($arResult);
    file_put_contents($filename, $data);
}
// Data output
$dataBaseCharset = $arResult['DATABASE_CHARSET'];
echo('<h3>DATABASE CHARSET: <q style="color: red">' . $dataBaseCharset . '</q></h3>');
foreach ($arResult['TABLES'] as $table_name => $val):
    echo('
        <hr>
        <h3 style="margin-left: 10px">Table name: "' . $table_name . '" </h3>
        <h4 style="margin-left: 20px">Properties: ' . $val['PROPERTY']['ENGINE'] . ' , ' . $val['PROPERTY']['CHARSET'] . '</h4>
        <h4 style="margin-left: 20px">Fields:</h4>');
    echo '<ul>';
        foreach ($val['FIELDS'] as $colum_name => $field) :
            echo '<li style="list-style-type: none;">
                        COLUMN_NAME: <b> ' . $colum_name . ' </b>,
                        COLUMN_TYPE: <b> ' . $field['COLUMN_TYPE'] . ' </b>,
                        CHARACTER_SET_NAME:';
                        echo '<b>';
                        if (($field['CHARACTER_SET_NAME'] == "NULL")) echo $field['CHARACTER_SET_NAME']; else echo('<q style="color: red">' . $field['CHARACTER_SET_NAME'] . '</q>');
                        echo '</b>';
            echo '</li>';
        endforeach;
    echo '</ul>';
endforeach;
