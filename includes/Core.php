<?php
class Application {
    public $arResult = array();
    private $tables = array();
    private $dbhost = "";
    private $dbname = "";
    private $dbuser = "";
    private $dbpass = "";

    function __construct($dbhost,$dbname,$dbuser,$dbpass = "")
    {
        $this->dbhost = $dbhost; // Адрес сервера MySQL. На локальном сервере этот параметр всегда будет 'localhost', но на хостинге он соответствует адресу хостера.
        $this->dbname = $dbname; // Имя базы данных
        $this->dbuser = $dbuser; // Пользователь базы данных
        $this->dbpass = $dbpass; // Пароль пользователя базы данных
        mysql_connect($dbhost, $dbuser, $dbpass) or die("Ошибка MySQL: " . mysql_error());
        mysql_select_db($dbname) or die("Ошибка MySQL: " . mysql_error());
    }

    private function getDatabaseCharset () {
        $result = array();
        $query = "SHOW VARIABLES LIKE 'character_set_database'";
        $query_result = mysql_query($query) or die ("Error : " . mysql_error());
        $result = mysql_fetch_array($query_result)[1];
        return $result;
    }

    function getTables (){
        $result = array();
        $query_tables = "SHOW TABLES";
        $result_tables = mysql_query($query_tables) or die ("Error : " . mysql_error());
        while ($rows_tables = mysql_fetch_array($result_tables)) {
            array_push($result,$rows_tables);
        }
        return $result;
    }

    private function getTableProps ($table_name){
        $result = array();
        $query_table = "SHOW CREATE TABLE `$table_name`";
        $result_table = mysql_query($query_table) or die ("Error : " . mysql_error());
        while ($rows_table = mysql_fetch_array($result_table)) {
            array_push($result,$rows_table);
        }
        return $result;
    }

    private function getTableFields ($table_name){
        $result = array();
        $query_fields = "SELECT `COLUMN_NAME`, `CHARACTER_SET_NAME`, `COLUMN_TYPE` FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$table_name'";
        $result_fields = mysql_query($query_fields) or die ("Error : " . mysql_error());
        while ($rows_fields = mysql_fetch_array($result_fields)) {
            array_push($result,$rows_fields);
        }
        return $result;
    }

    public function getResult($table_name = "all"){
        if ($table_name == "all")
            $this->tables = $this->getTables();
        else
            $this->tables[0][0] = $table_name;
        $dataBaseCharset = $this->getDatabaseCharset();
        $this->arResult["DATABASE_CHARSET"] = $dataBaseCharset;
        foreach ($this->tables as $table_name):
            $table_name = $table_name[0];
            foreach ($table_props = $this->getTableProps($table_name) as $property) :
                $engine = stristr($property[1], 'ENGINE');
                $engine = substr($engine, 0, stripos($engine, ' '));
                $charset = stristr($property[1], 'CHARSET');
                if (!!stripos($charset, ' '))
                    $charset = substr($charset, 0, stripos($charset, ' '));
                $this->arResult['TABLES'][$table_name]["PROPERTY"]["ENGINE"] = $engine;
                $this->arResult['TABLES'][$table_name]["PROPERTY"]["CHARSET"] = $charset;
            endforeach;
            $arFields = array();
            foreach ($table_fields = $this->getTableFields($table_name) as $field) :
                $arFields[$field['COLUMN_NAME']]['COLUMN_TYPE'] = $field['COLUMN_TYPE'];
                $arFields[$field['COLUMN_NAME']]['CHARACTER_SET_NAME'] = ((is_null($field['CHARACTER_SET_NAME'])) ? "NULL" : $field['CHARACTER_SET_NAME']);
            endforeach;
            $this->arResult['TABLES'][$table_name]["FIELDS"] = $arFields;
        endforeach;
        return $this->arResult;
    }
}
?>