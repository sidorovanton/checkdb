function update() {
    var table = $('#tables_list').attr('value');
    $.ajax({
        type: 'POST',
        url: '/includes/result.php',
        data: ({table_name: table}),
        success: function (data) {
            $("#content").empty().append(data);
        }
    });
}
function appendOptions() {
    $.ajax({
        type: 'POST',
        url: '/includes/result.php',
        data: ({options:true}),
        success: function(data){
            $("#tables_list").empty().append(data);
            update();
        }
    });
}
function save(file_name) {
    var table = $('#tables_list').attr('value');
    $.ajax({
        type: 'POST',
        url: '/includes/result.php',
        data: ({table_name: table, save: true,file_name:file_name}),
        success: function(data){
            $('#save_button').val('Saved').attr("style","color:green");
        }
    });
}
function open(file_name) {
    $.ajax({
        type: 'POST',
        url: '/includes/result.php',
        data: ({file_name:file_name,open:true}),
        success: function(data){
            if (!data){
                $('#alert').text("File not exists").attr("style","color:red").show();
            }
            else{
                $("#content").empty().append(data);
                $('#open_button').val('Opened').attr("style","color:green");
            }

        }
    });
}
$(document).ready( function() {
    appendOptions();
    $('#tables_list').bind('change', function() {
        update();
    });

    $('#save_button').bind('click', function() {
        $('#save_button').val('Save').removeAttr("style","color:green").hide();
        $('#open_button').val('Open').removeAttr("style","color:green").hide();
        $('#file_name').show();
        $('#ok_button_save').show();
        $('#cancel_button').show();
    });
    $('#open_button').bind('click', function() {
        $('#save_button').val('Save').removeAttr("style","color:green").hide();
        $('#open_button').val('Open').removeAttr("style","color:green").hide();
        $('#file_name').show();
        $('#ok_button_open').show();
        $('#cancel_button').show();
    });

    $('#cancel_button').bind('click', function() {
        $('#save_button').show();
        $('#open_button').show();
        $('#file_name').hide();
        $('#ok_button_save').hide();
        $('#ok_button_open').hide();
        $('#cancel_button').hide();
        $('#alert').text('').hide();
    });

    $('#ok_button_save').bind('click', function() {
        if ($('#file_name').val().length != 0) {
            $('#save_button').show();
            $('#open_button').show();
            $('#file_name').hide();
            $('#ok_button_save').hide();
            $('#ok_button_open').hide();
            $('#cancel_button').hide();
            $('#alert').text("").hide();
            save($('#file_name').val());
        }
        else {
            $('#alert').text("Enter the file name").attr("style","color:red").show();
        }
    });
    $('#ok_button_open').bind('click', function() {
        if ($('#file_name').val().length != 0) {
            $('#save_button').show();
            $('#open_button').show();
            $('#file_name').hide();
            $('#ok_button_save').hide();
            $('#ok_button_open').hide();
            $('#cancel_button').hide();
            $('#alert').text("").hide();
            open($('#file_name').val());
        }
        else {
            $('#alert').text("Enter the file name").attr("style","color:red").show();
        }
    });

});
