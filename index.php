<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>DB Checker</title>
    <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script type="text/javascript" src="includes/ajax.js"></script>
</head>
<body>
<select id="tables_list"></select>
<input type="button" id="save_button" value="Save"/>
<input type="button" id="open_button" value="Open"/>
<input type="text" id="file_name" style="display: none;" placeholder="File name"/>
<input type="button" id="ok_button_save" style="display: none;" value="Ok"/>
<input type="button" id="ok_button_open" style="display: none;" value="Ok"/>
<input type="button" id="cancel_button" style="display: none;" value="Cancel"/>
<span id="alert" style="display: none;"></span>
<div id="content"></div>
</body>
</html>
